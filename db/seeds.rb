# frozen_string_literal: true

Admin.create!(
  email: 'admin@test.com',
  password: 'DRjfi7989ds4'
)

book_list = [
  'Гордость и предубеждение, Джейн Остен',
  '1984, Джордж Оруэлл',
  'Великий Гэтсби, Ф. С. Фицджеральд',
  'Маленькие женщины, Луиза Мэй Олкотт',
  '451° по Фаренгейту, Рэй Брэдбери',
  'Джейн Эйр, Шарлотта Бронте',
  'Унесенные ветром, Маргарет Митчелл',
  'Скотный двор, Джордж Оруэлл'
].map do |item|
  data = item.split(',')
  { title: data[0], author: data[1] }
end

Book.insert_all(book_list)
