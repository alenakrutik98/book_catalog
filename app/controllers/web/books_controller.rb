# frozen_string_literal: true

module Web
  ##
  # Управление книгами
  class BooksController < ApplicationController
    before_action :authenticate_user!, except: %i[index show]
    before_action :set_book, only: %i[show edit update destroy]

    def index
      @books = Book.all
    end

    def new
      @book = Book.new
    end

    def create
      @book = Book.new(book_params)

      if @book.save
        respond_to do |format|
          format.html { redirect_to books_path, notice: t('success_create') }
          format.turbo_stream { flash.now[:notice] = t('success_create') }
        end
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @book.update(book_params)
        redirect_to book_path(@book), notice: t('success_update')
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @book.destroy

      respond_to do |format|
        format.html { redirect_to quotes_path, notice: t('success_destroy') }
        format.turbo_stream { flash.now[:notice] = t('success_destroy') }
      end
    end

    private

    def set_book
      @book = Book.find(params[:id])
    end

    def book_params
      params.require(:book).permit(:author, :title, :description)
    end
  end
end
