# frozen_string_literal: true

##
# Пользователи системы
class User < ApplicationRecord
  devise :database_authenticatable, :validatable

  def name
    email.split('@').first.capitalize
  end
end
