# frozen_string_literal: true

class Book < ApplicationRecord
  has_many :comments, dependent: :destroy

  validates :title, presence: true
  validates :author, presence: true

  broadcasts_to ->(_book) { 'books' }, insert_by: :prepend
end
