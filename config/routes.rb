# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  root to: 'web/books#index'

  scope module: 'web' do
    resources :books  do
      resources :comments
    end
  end
end
